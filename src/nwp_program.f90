!------------------------------------------------------------------------------!
! Numerical Weather Prediction - Programming Excercise
!------------------------------------------------------------------------------!
! Tolles Programm für das Praktikum

! TODO: Add description
!  Add a proper description of program. Should include how to steer (including
!  required input) and what to expect as output.
!  2018-02-14, TG

!------------------------------------------------------------------------------!
! Author: Tobias Gronemeier
! Date  : 2018-02-14
!------------------------------------------------------------------------------!
! NOTE: Code compilation
!  When compiling the code, NetCDF libraries must be included/linked like so
!    ifort nwp_program.f90 -I <path-to-netcdf>/include -L <path-to-netcdf>/lib -lnetcdff
!  2018-02-07, TG

!ifort -o <Programmname> <Dateiname>.f90 -I  /muksoft/packages/netcdf4_hdf5parallel/4411c_443f/hdf5-1.10.0-patch1/mvapich2-2.3rc1/intel/2018.1.163/include -L /muksoft/packages/netcdf4_hdf5parallel/4411c_443f/hdf5-1.10.0-patch1/mvapich2-2.3rc1/intel/2018.1.163/lib -lnetcdff -g




PROGRAM nwp_program

   USE netcdf

   IMPLICIT NONE

!------------------------------------------------------------------------------
!-variablen Deklarieren

   CHARACTER(LEN=100) :: file_in 
   CHARACTER(LEN=100) :: file_out = 'nwp_output' !< name of output file 

   INTEGER :: i               	!< x Laufindex
   INTEGER :: j               	!< y Laufindex
   INTEGER :: k               	!< Iteration Laufindex
   INTEGER :: t = 0           	!< zeitl. Laufindex
   INTEGER :: t_do = 0
   INTEGER :: k_max = 1000    	!< Maximale Iterationen der SOR-Methode
   INTEGER :: nx = 10         	!< Anzahl der Gitterpunkte in x-Richtung 
   INTEGER :: ny = 10         	!< Anzahl der Gitterpunkte in y-Richtung 
   INTEGER :: nt              	!< Anzahl der Zeitschritte
   INTEGER :: nt_ref = 8	  	!< Anzahl der Zeitschritte von phi (8*6h=48h)
   INTEGER :: scalar_select	  	!< Parameter um verschiedene Fälle auszusuchen
   INTEGER :: ioerr			  	!< Fehlerbedingung
   INTEGER :: count, count_rate	!< Messung der Rechenzeit
   INTEGER :: t_ref = 0         !< zeitl. Laufindex 
   INTEGER :: t_do_ref = 0

   REAL :: beta = 1.62E-11    	!< beta Parameter
   REAL :: delta = 30000.0    	!< Gitterweite [m]
   REAL :: dt                 	!< Länge des Zeitschritts [s]
   REAL :: f_0 = 0.0001       	!< Coriolis Parameter bei y=0
   REAL :: latitude = 52.0    	!< geographische Breite [°]
   REAL :: omega              	!< Konvergenzfaktor der SOR-Methode
   REAL :: t_end              	!< zu simulierende Zeit [s]
   REAL :: time_end = 24.0    	!< zu simulierende Zeit [h]
   REAL :: t_sim = 0.0        	!< bereits simulierte Zei [s]
   REAL :: time_dt_do = 24.0  	!< Zeitintervall des Outputs [h]
   REAL :: x, y					!< Positions Angabe
   REAL :: x_s, y_s             !< Position des Skalars
   REAL :: diff1, diff2			
   REAL :: diff3, diff4			!< Parameter zur bestimmung der Rechenzeit
   REAL :: diff5
   REAL :: diff2_ges
   REAL :: diff3_ges			!< Parameter zur best. der Gesamtrechenzeit
   REAL :: diff4_ges
   REAL :: diff5_ges

   REAL, DIMENSION(:), ALLOCATABLE :: f         !< Coriolis parameter along y
   REAL, DIMENSION(:), ALLOCATABLE :: ff		!< Windgeschwindigkeit
   REAL, DIMENSION(:), ALLOCATABLE :: dd		!< Windrichtung
   REAL, DIMENSION(:), ALLOCATABLE :: phi_loc	!< Geopotential an best. Ort
   REAL, DIMENSION(:), ALLOCATABLE :: s_loc     !< Skalar an einem best. Ort

   REAL, DIMENSION(:,:), ALLOCATABLE :: phi     !< Geopotential
   REAL, DIMENSION(:,:), ALLOCATABLE :: u       !< wind Geschw. in x-Richtung
   REAL, DIMENSION(:,:), ALLOCATABLE :: v       !< wind Geschw. in y-Richtung
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta    !< Vorticity zur Zeit t
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_m  !< Vorticity zur Zeit t-1
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_p  !< Vorticity zur Zeit t+1
   REAL, DIMENSION(:,:), ALLOCATABLE :: s       !< Aschegehalt

   REAL, DIMENSION(:,:,:), ALLOCATABLE ::phi_ref!< Referenz Geopotential

   REAL, PARAMETER :: g = 9.81      			!< Erdbeschleunigung
   REAL, PARAMETER :: pi = 3.14159  			!< Pi


!------------------------------------------------------------------------------
! -------------------------------- Start--------------------------------------
!------------------------------------------------------------------------------   

!-Zeiterfassung für gesamtes Programm

   CALL SYSTEM_CLOCK (count, count_rate)
   diff1 = REAL(count)/REAL(count_rate)


   WRITE(*, '(A//A)') ' --- NWP program, V0.0.1 ---',  &
                      ' --- start forecast...'


!------------------------------------------------------------------------------
!-Namelist definieren, öffnen und lesen

   namelist / INIT / delta, file_in, file_out, k_max, latitude, nx, ny, &
					 time_dt_do, time_end, scalar_select, x, y, x_s, y_s

   OPEN(1, FILE='/home/brassat/Documents/Git/nwp_template/input/nwp_prog.init'&
        , STATUS='OLD', FORM='FORMATTED', IOSTAT = ioerr)
		
   IF (ioerr /= 0) THEN
      CALL error_message
   ENDIF

   READ(1, NML=INIT, IOSTAT = ioerr)
   IF (ioerr /= 0) THEN
      CALL error_message
   ENDIF

   CLOSE(1)
   
!------------------------------------------------------------------------------
!-Outputfile 
   OPEN(2, FILE='/home/brassat/Documents/Git/nwp_template/output/output.txt' &
        , ACTION='WRITE')
   
   WRITE(2,15) 'Daten für den Ort ', x /1000.0, ' km, ', y/1000.0, ' km'
   
   SELECT CASE (scalar_select)
    
    CASE(1)
        WRITE(2,'(A)') 'ohne Advektion eines Skalars'
    CASE(2)
        WRITE(2,15) 'mit Advektion eines Skalars bei ', x_s/1000.0, ' km, ' &
                    ,y_s/1000.0, ' km '
    CASE(3)
        WRITE(2,15) 'mit Advektion eines kontinuierlichen Skalars bei ' &
                    , x_s/1000.0, ' km, ',y_s/1000.0, ' km '
   END SELECT
   
   WRITE(2,*)'-------------------------------------------------------------'
   WRITE(2,13) 't [h]','   phi[m²/s²]','  ff[m/s]','  dd[°]','     s'

   13 FORMAT (5A)
   15 FORMAT (A, F6.1, A, F6.1 ,A)
!------------------------------------------------------------------------------
!-Initialisieren und Allokieren von Parametern und Feldern

!-Programmlaufzeit
   diff2_ges = 0.0                                            
   diff3_ges = 0.0
   diff4_ges = 0.0

!-Konvergenzfaktor der SOR-Methode
   omega = 2.0 - 2.0 * pi / SQRT(2.0) * SQRT(1.0 / (nx + 1.0) ** 2.0 &
		   + 1.0 / (ny + 1.0) ** 2.0) 						  

!-Anzahl der 30 min. Schritte 
   t = INT(time_end * 60 / 30.0) - 1
   
!-Allokieren der Felder   
   ALLOCATE(f(0:ny))                                          			
   ALLOCATE(phi(-1:ny+1,-1:nx+1))
   ALLOCATE(phi_ref(-1:ny+1,-1:nx+1,0:nt_ref))
   ALLOCATE(zeta(-1:ny+1,-1:nx+1))
   ALLOCATE(zeta_m(-1:ny+1,-1:nx+1))
   ALLOCATE(zeta_p(-1:ny+1,-1:nx+1))
   ALLOCATE(u(-1:ny+1,-1:nx+1))
   ALLOCATE(v(-1:ny+1,-1:nx+1))
   ALLOCATE(s(-1:ny+1,-1:nx+1))
   
   ALLOCATE(ff(0:t))
   ALLOCATE(dd(0:t))
   ALLOCATE(phi_loc(0:t))
   ALLOCATE(s_loc(0:t))
   
   s(:,:) = 0.0
   
!-Beta und Coriolisparameter abhängig vom Breitengrad berechnen
   beta = ( 2.0 / 6367490.0 ) * ( 2.0 * pi / 86164.0 ) * &
		  COS(latitude * pi / 180.0)  						
   
   f_0 = 4.0 * pi * SIN(latitude * pi / 180.0) / 86164.0
   
   DO j = 0 , ny
    f(j) = f_0 + beta * delta * j 
   END DO
   
! Stunden in Sekunden umrechnen
   t_end = time_end * 3600.0 ![t_end]=s
 
!------------------------------------------------------------------------------
!-Anfangswerte bestimmen

!- Input-Datei lesen
   CALL read_input_data(file_in)


!- Output-Datei öffnen
   CALL data_output('open', file_out)
   
   phi(:,:) = phi_ref(:,:,0)
   
!- Anfangswert der Vorticity mit Poisson-Glg. berechnen      
    DO i=0,nx
        DO j=0,ny

            zeta(j,i) = (1 / f(j)) * (((phi_ref(j,i+1,0) - 2.0 * &
                        phi_ref(j,i,0) + phi_ref(j,i-1,0)) / (delta ** 2)) &
						+ ((phi_ref(j+1,i,0) - 2.0 * phi_ref(j,i,0)&
						+ phi_ref(j-1,i,0)) / (delta ** 2))) 
				
        END DO
    END DO
   
   zeta_p = zeta
   zeta_m = zeta
   
   CALL get_wind
   IF (scalar_select > 1) THEN
    CALL get_scalar
   END IF
   CALL get_location
   CALL data_output('write', file_out)
   
   
!------------------------------------------------------------------------------
!-Vorhersage

   t_sim = 0.0
   
   dt = timestep()
   
   t = 0

!-Prognostische Schleife

   DO WHILE (t_sim <= t_end)
        
    t = t + 1
    dt = timestep()
    nt = INT(t_end/dt)
    t_ref = INT(t_sim/3600.0/6.0)

!-Berechnen der Werte    
    CALL get_border_values  
    CALL get_vorticity
    CALL get_geopot(k_max)
    CALL get_wind
    
    IF (scalar_select > 1) THEN
            
        CALL get_scalar_advection
        
    END IF
        
    CALL get_location

!-Output der Datei abh. vom Zeitintervall   
    t_sim = t_sim + dt
    
    t_do = INT(t_sim / (time_dt_do * 3600.0))

    IF (t_do /= t_do_ref) THEN
        
        CALL data_output('write', file_out)
        
    END IF
    
    t_do_ref = t_do
        
        
    WRITE(*,*) 'Schritt', t, 'von', nt
        
   END DO

!-Output finalisieren
   CALL data_output('close', file_out)

!------------------------------------------------------------------------------
!-Fertig

   WRITE(*, '(A)') ' --- Finished! :-) ---'
 
!-Maximal-/ und Minimalwerte eines bestimmten Ortes angeben 
   CALL get_location_maxmin
   
!------------------------------------------------------------------------------
!-Zeiterfassung
   
    CALL SYSTEM_CLOCK (count, count_rate)
    diff1 = REAL(count)/REAL(count_rate) - diff1
    WRITE(2,'(A)') '----Rechenzeit----'
    WRITE(2,14) 'Gesamtzeit Programm: ', diff1, ' s ', 100.00,'  %' 
    WRITE(2,14) 'Gesamtzeit Zeitschritte: ', diff2_ges,' s ', &
                (diff2_ges/diff1)*100.0, ' %'
    WRITE(2,14) 'Gesamtzeit SOR Verfahren: ', diff3_ges,' s ', &
                (diff3_ges/diff1)*100.0, ' %'
    WRITE(2,14) 'Gesamtzeit Wind: ', diff4_ges,' s ', &
                (diff4_ges/diff1)*100.0, ' %'
    WRITE(2,14) 'Gesamtzeit Output: ', diff5_ges,' s ',&
                (diff5_ges/diff1)*100.0, ' %'
    
    14 FORMAT (A, F7.4, A, F8.4,A)

    CLOSE(2)
    
!------------------------------------------------------------------------------
! ---------------------------------ENDE---------------------------------------
!------------------------------------------------------------------------------


!-Funktionen und Subroutinen
CONTAINS

!------------------------------------------------------------------------------
!-Funktion für Zeitschritt Delta t abhängig vom maximalen Wind (CFL-Kriterium)

   REAL FUNCTION timestep()
    REAL, DIMENSION (2) :: cfl_min
        cfl_min(1) = delta / MAXVAL(ABS(u))
        cfl_min(2) = delta / MAXVAL(ABS(v))
        
        IF (scalar_select == 1) THEN
            timestep = 0.5 * MINVAL(ABS(cfl_min))
        ELSE
            timestep = 0.25 * MINVAL(ABS(cfl_min))
        END IF
    
   END FUNCTION timestep

!------------------------------------------------------------------------------
!-Fehlermeldungen

   SUBROUTINE error_message
    WRITE(*,*) 'Namelist Fehler'
    STOP
   END SUBROUTINE error_message

!------------------------------------------------------------------------------
!-Berechnung des Geopotentials mit der SOR-Methode

   SUBROUTINE get_geopot(max_it)
   
    INTEGER, INTENT (IN) :: max_it  !< maximum iteration count

    REAL :: differenz, diff_alt, grenzwert
    REAL, DIMENSION(-1:ny+1,-1:nx+1) :: phi_alt     !< geopotential

    CALL SYSTEM_CLOCK (count, count_rate)
    diff3 = REAL(count)/REAL(count_rate)    
    
    grenzwert = 0.001
    diff_alt = -99.0

    phi_alt(:,:) = phi(:,:)
      	  
!-SOR-Verfahren

	DO k = 1,k_max
        DO i = 0,nx
			DO j = 0,ny

				phi(j,i) = phi(j,i) * (1.0 - omega) + 0.25 * omega &
                           * (phi(j,i+1) + phi(j,i-1) + phi(j+1,i) &
                           + phi(j-1,i) - f(j) * zeta(j,i) * delta ** 2.0)
				
			END DO
		END DO
		
		differenz = MAXVAL(ABS(phi-phi_alt))
		
		IF (differenz <= diff_alt .AND. differenz <= grenzwert) THEN
			WRITE(*,*) 'Iteration', k
			EXIT
		END IF
		
		
		diff_alt = differenz
		phi_alt(:,:) = phi(:,:)

    END DO
	  
      CALL SYSTEM_CLOCK (count, count_rate)
      diff3 = REAL(count)/REAL(count_rate)-diff3
      diff3_ges = diff3_ges + diff3
      
      !WRITE(*,*) 'SOR Zeit:', diff3
	  
   END SUBROUTINE get_geopot

!------------------------------------------------------------------------------
!-Berechnung der Vorticity mit Prognistischer Gleichung

   SUBROUTINE get_vorticity
   
    CALL SYSTEM_CLOCK (count, count_rate)
    diff2 = REAL(count)/REAL(count_rate)   
    

    SELECT CASE (t)
    
!-Eulermethode für ersten Schritt   
    CASE(1)
        DO i = 1,nx-1
            DO j = 1,ny-1
                zeta_p(j,i) = zeta(j,i) + dt * (-u(j,i) * (zeta(j,i+1) &
                              - zeta(j,i-1)) / (2.0 * delta) - v(j,i) &
                              * (zeta(j+1,i) - zeta(j-1,i)) / (2.0 * delta) &
                              -v(j,i) * beta)
            END DO
        END DO
    
!-Leapfrog-Methode ab zweiten Schritt
     CASE(2:)
        DO i = 1,nx-1
            DO j = 1,ny-1
                zeta_p(j,i) = zeta_m(j,i) + 2.0 * dt * (-u(j,i) &
                              * (zeta(j,i+1) - zeta(j,i-1)) / (2.0 * delta) &
                              - v(j,i) * (zeta(j+1,i) - zeta(j-1,i)) &
                              / (2.0 * delta) - v(j,i) * beta)
            END DO
        END DO
    
    END SELECT
    
    
    
    zeta_m(:,:) = zeta(:,:)
    zeta(:,:) = zeta_p(:,:)    
        
        
    CALL SYSTEM_CLOCK (count, count_rate)    
    diff2 = REAL(count)/REAL(count_rate) - diff2
    diff2_ges = diff2_ges + diff2  
    
    !WRITE(*,*) 'Zeit Vorticity:', diff2
    
   END SUBROUTINE get_vorticity

!------------------------------------------------------------------------------
!-Berechnung des Windes mit der Gleichung für den geostrophischen Wind

   SUBROUTINE get_wind
   
      CALL SYSTEM_CLOCK (count, count_rate)
      diff4 = REAL(count) / REAL(count_rate)
         
      DO i = 0,nx
        DO j = 0,ny
            u(j,i) = - (phi(j+1,i) - phi(j-1,i)) / (2.0 * delta * f(j))
            v(j,i) = (phi(j,i+1) - phi(j,i-1)) / (2.0 * delta * f(j))
        END DO
      END DO  
      
      CALL SYSTEM_CLOCK (count, count_rate)
      diff4 = REAL(count)/REAL(count_rate) - diff4
      diff4_ges=diff4_ges + diff4
      
      !WRITE(*,*) 'Zeit Wind', diff4 
      
   END SUBROUTINE get_wind
   
!------------------------------------------------------------------------------
!-Randwerte berechnen
   
   SUBROUTINE get_border_values 
 
!-Zeitliche Interpolation der Randwerte vom Geopotential   
    phi(-1:1,:) = phi_ref(-1:1,:,t_ref) + (phi_ref(-1:1,:,t_ref+1) &
                  - phi_ref(-1:1,:,t_ref)) / (21600.0) * (t_sim-t_ref * 21600.)
   
    phi(ny-1:ny+1,:) = phi_ref(ny-1:ny+1,:,t_ref) &
                       + (phi_ref(ny-1:ny+1,:,t_ref+1) &
                       - phi_ref(ny-1:ny+1,:,t_ref)) / (21600.0) &
                       * (t_sim-t_ref * 21600.0)
    
    phi(:,-1:1) = phi_ref(:,-1:1,t_ref) + (phi_ref(:,-1:1,t_ref+1) &
                  - phi_ref(:,-1:1,0)) / (21600.0) * (t_sim-t_ref * 21600.0)
    
    phi(:,nx-1:nx+1) = phi_ref(:,nx-1:nx+1,t_ref) &
                       + (phi_ref(:,nx-1:nx+1,t_ref+1) &
                       - phi_ref(:,nx-1:nx+1,t_ref)) / (21600.0) &
                       * (t_sim-t_ref * 21600.0)

!-Berechnung des Rands der Vorticity mit der Poisson-Glg.
    DO i=0,nx
        zeta(0,i)=(1 / f(0)) * (((phi(0,i+1) - 2.0 * phi(0,i) &
                  + phi(0,i-1)) / (delta ** 2)) + ((phi(1,i) - 2.0 * phi(0,i) &
                  + phi(-1,i))/(delta ** 2)))
    END DO
    
    DO i = 0,nx
        zeta(ny,i)=(1 / f(ny)) * (((phi(ny,i+1) - 2.0 * phi(ny,i) &
                   + phi(ny,i-1)) / (delta**2)) + ((phi(ny+1,i) &
                   - 2.0 * phi(ny,i) + phi(ny-1,i)) / (delta ** 2))) 
    END DO
    
    DO j = 0,ny
        zeta(j,0) = (1 / f(j)) * (((phi(j,1) - 2.0 * phi(j,0) &
                    + phi(j,-1)) / (delta ** 2)) + ((phi(j+1,0) &
                    - 2.0 * phi(j,0) + phi(j-1,0)) / (delta ** 2)))
    END DO
    
    DO j = 0,ny
        zeta(j,nx) = (1 / f(j)) * (((phi(j,nx+1) - 2.0 * phi(j,nx) &
                     + phi(j,nx-1)) / (delta ** 2)) + ((phi(j+1,nx) &
                     - 2.0 * phi(j,nx) + phi(j-1,nx)) / (delta ** 2)))  
    END DO

   END SUBROUTINE get_border_values

   
!------------------------------------------------------------------------------
!-Vorhersage an einem bestimmten Punkt

   SUBROUTINE get_location
   
    INTEGER :: i_x, j_y
    INTEGER :: t_loc 
    INTEGER :: t_loc_diff = -1
    
    t_loc = INT(t_sim/1800.0)
    
    i_x = INT(delta/x)
    j_y = INT(delta/y)
    
    ff(t_loc) = SQRT(u(j_y,i_x) ** 2.0 + v(j_y,i_x) ** 2.0)
    dd(t_loc) = ATAN2(u(j_y,i_x),v(j_y,i_x)) * 180.0 / pi
    phi_loc(t_loc) = phi(j_y,i_x)
    s_loc(t_loc) = s(j_y,i_x)
    
    IF (t_loc_diff /= t_loc) THEN
        WRITE(2,11) t_loc*0.5, phi_loc(t_loc), ff(t_loc), dd(t_loc) &
                    , s_loc(t_loc)
    END IF    
    
    t_loc_diff = t_loc
    
    11 FORMAT (F4.1, F13.2, F9.2, F9.2, F13.2)
    
   END SUBROUTINE
   

!------------------------------------------------------------------------------
!-Max. und Min. an einem bestimmten Punkt

   SUBROUTINE get_location_maxmin
   
    REAL :: ff_max, ff_min, t_ff_max, t_ff_min
    REAL :: dd_max, dd_min, t_dd_max, t_dd_min
    REAL :: phi_loc_max, phi_loc_min, t_phi_max, t_phi_min
    REAL :: s_max, s_min, t_s_max, t_s_min
    
    ff_max = MAXVAL(ff)
    ff_min = MINVAL(ff)
    t_ff_max = MAXVAL(MAXLOC(ff)) * 0.5
    t_ff_min = MAXVAL(MINLOC(ff)) * 0.5
    
    dd_max = MAXVAL(ABS(dd))
    dd_min = MINVAL(ABS(dd))
    t_dd_max = MAXVAL(MAXLOC(dd)) * 0.5
    t_dd_min = MAXVAL(MINLOC(dd)) * 0.5
    
    phi_loc_max = MAXVAL(phi_loc)
    phi_loc_min = MINVAL(phi_loc)
    t_phi_max = MAXVAL(MAXLOC(phi_loc)) * 0.5
    t_phi_min = MAXVAL(MINLOC(phi_loc)) * 0.5
    
    s_max = MAXVAL(s_loc)
    s_min = MINVAL(s_loc)
    t_s_max = MAXVAL(MAXLOC(s_loc)) * 0.5
    t_s_min = MAXVAL(MINLOC(s_loc)) * 0.5    

    WRITE(2,*) '-------------------------------------------------------------'
    WRITE(2,10) 'Maximales Geopotential:', phi_loc_max, ' nach ' &
                , t_phi_max, ' h'  
    WRITE(2,10) 'Minimales Geopotential:', phi_loc_min, ' nach ' &
                , t_phi_min, ' h' 
    
    WRITE(2,9) 'Maximaler Wind:', ff_max, ' nach ', t_ff_max, ' h'  
    WRITE(2,9) 'Minimaler Wind:  ', ff_min, ' nach ', t_ff_min, ' h'  
    
    WRITE(2,10) 'Maximale Windrichtung:', dd_max, ' nach ', t_dd_max, ' h'  
    WRITE(2,10) 'Minimale Windrichtung:', dd_min, ' nach ', t_dd_min, ' h'  
    
    WRITE(2,10) 'Maximale Asche konz.:', s_max, ' nach ', t_s_max, ' h'  
    WRITE(2,10) 'Minimale Asche konz.:', s_min, ' nach ', t_s_min, ' h'     
    WRITE(2,10) '-------------------------------------------------------------'
    
    9 FORMAT (A, F7.3, A, F4.1,A)
    10 FORMAT (A, F10.3, A, F4.1,A)
    
   END SUBROUTINE
   
   

!------------------------------------------------------------------------------
!-Anfangswert eines Skalars

   SUBROUTINE get_scalar
   
    INTEGER :: i_q, j_q
   
    REAL :: a = 10.0E11
    REAL :: alpha
    
    i_q = INT(x_s / delta)
    j_q = INT(y_s / delta)
    
    alpha = 0.005
    
    s(:,:) = 0.0
    
    
    DO i = 1,nx-1
        DO j = 1,ny-1
            
            s(j,i) = (a / (2 * pi)) * EXP(-alpha * ((i - i_q) ** 2 &
                     + (j - j_q) ** 2)) 
        
        END DO
    END DO
   
   END SUBROUTINE
   

!------------------------------------------------------------------------------
!-Advektion eines Skalars

   SUBROUTINE get_scalar_advection
    
    REAL, DIMENSION(-1:ny+1,-1:nx+1) :: q
    REAL, DIMENSION(-1:ny+1,-1:nx+1) :: s_p
    INTEGER :: i_q, j_q
    
    i_q = INT(x_s / delta)
    j_q = INT(y_s / delta)
 
    q(:,:) = 0.0
    
    IF (scalar_select > 2) THEN
        q(j_q,i_q) = (2.0E9  / 3600.0) * dt
    END IF
    
    DO i = 1,nx-1
        DO j = 1, ny-1
            
            s_p(j,i) = s(j,i) - dt * (-q(j,i) + ((u(j,i) + ABS(u(j,i)) / 2.0) &
                       * ((s(j,i) & - s(j,i-1)) / delta)) + ((u(j,i) -&
                       ABS(u(j,i)) / 2.0) * ((s(j,i+1) - s(j,i)) / delta)) &
                       + ((v(j,i) + ABS(v(j,i)) / 2.0) * ((s(j,i) - s(j-1,i)) &
                       / delta)) + ((v(j,i) - ABS(v(j,i)) / 2.0) * ((s(j+1,i) &
                       - s(j,i)) / delta)))
        END DO
    END DO
    
    s(:,:) = s_p(:,:)
    
   END SUBROUTINE
   
   
!------------------------------------------------------------------------------
!-Lesen der Input-Datei des Mesoskaligen Modells

   SUBROUTINE read_input_data(input)
   

      CHARACTER(LEN=*), INTENT(IN) :: input  !< name of input file

      INTEGER, SAVE :: id_file         !< ID of NetCDF file
      INTEGER, SAVE :: id_var_in       !< ID of input array
      INTEGER, SAVE :: nc_stat         !< status flag of NetCDF routines

      REAL, DIMENSION(-1:nx+1,-1:ny+1,0:nt_ref) :: var_in   !< input array

      WRITE(*, '(A)') ' --- Read input file...'

      !- Open NetCDF file
      nc_stat = NF90_OPEN( input, NF90_NOWRITE, id_file )

      !- Get ID of array to read
      nc_stat = NF90_INQ_VARID( id_file, "phi", id_var_in )

      !- Read array
      nc_stat = NF90_GET_VAR( id_file, id_var_in, var_in,        &
                            start = (/ 1, 1, 1 /),               &
                            count = (/ nx+3, ny+3, nt_ref+1 /) )

      !- Resort input array and save to proper variable
      DO  t = 0, nt_ref
         DO  i = -1, nx+1
            DO  j = -1, ny+1
               phi_ref(j,i,t) = var_in(i,j,t)
            ENDDO
         ENDDO
      ENDDO

      WRITE(*, '(A)') '     ...finished'

   END SUBROUTINE read_input_data

!------------------------------------------------------------------------------
!-Daten Output in NCF Datei

   SUBROUTINE data_output(action, output)
   

      CHARACTER(LEN=*), INTENT(IN) :: action !< flag to steer routine (o/c/w)
      CHARACTER(LEN=*), INTENT(IN) :: output !< output file name prefix

      INTEGER, SAVE :: do_count=0      !< counting output
      INTEGER, SAVE :: id_dim_time     !< ID of dimension time
      INTEGER, SAVE :: id_dim_x        !< ID of dimension x
      INTEGER, SAVE :: id_dim_y        !< ID of dimension time
      INTEGER, SAVE :: id_file         !< ID of NetCDF file
      INTEGER, SAVE :: id_var_phi      !< ID of geopotential
      INTEGER, SAVE :: id_var_phiref   !< ID of reference geopotential
      INTEGER, SAVE :: id_var_time     !< ID of time
      INTEGER, SAVE :: id_var_u        !< ID of wind speed along x
      INTEGER, SAVE :: id_var_v        !< ID of wind speed along y
      INTEGER, SAVE :: id_var_x        !< ID of x
      INTEGER, SAVE :: id_var_y        !< ID of y
      INTEGER, SAVE :: id_var_zeta     !< ID of vorticity
      INTEGER, SAVE :: id_var_s
      INTEGER, SAVE :: id_var_ff
      INTEGER, SAVE :: id_var_dd
      INTEGER, SAVE :: id_var_philoc
      INTEGER, SAVE :: id_var_sloc
      INTEGER, SAVE :: nc_stat         !< status flag of NetCDF routines

      INTEGER :: tt                    !< time index of ref. geopot.

      REAL, DIMENSION(:), ALLOCATABLE :: netcdf_data_1d  !< 1D output array

      REAL, DIMENSION(:,:), ALLOCATABLE :: netcdf_data_2d  !< 2D output array

    CALL SYSTEM_CLOCK (count, count_rate)    
    diff5 = REAL(count)/REAL(count_rate)
      
      SELECT CASE (TRIM(action))

         !- Initialize output file
         CASE ('open')

            !- Delete any pre-existing output file
            OPEN(20, FILE=TRIM(output)//'.nc')
            CLOSE(20, STATUS='DELETE')

            !- Open file
            nc_stat = NF90_CREATE(TRIM(output)//'.nc', NF90_NOCLOBBER, id_file)
            IF (nc_stat /= NF90_NOERR)  PRINT*, '+++ netcdf error'

            !- Write global attributes
            nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL,  &
                                    'Conventions', 'COARDS')
            nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL,  &
                                    'title', 'barotropic nwp-model')

            !- Define time coordinate
            nc_stat = NF90_DEF_DIM(id_file, 'time', NF90_UNLIMITED,  &
                                    id_dim_time)
            nc_stat = NF90_DEF_VAR(id_file, 'time', NF90_DOUBLE,  &
                                    id_dim_time, id_var_time)
            nc_stat = NF90_PUT_ATT(id_file, id_var_time, 'units',  &
                                    'seconds since 2017-10-24 00:00:00')

            !- Define spatial coordinates
            nc_stat = NF90_DEF_DIM(id_file, 'x', nx+1, id_dim_x)
            nc_stat = NF90_DEF_VAR(id_file, 'x', NF90_DOUBLE,  &
                                    id_dim_x, id_var_x)
            nc_stat = NF90_PUT_ATT(id_file, id_var_x, 'units', 'meters')

            nc_stat = NF90_DEF_DIM(id_file, 'y', ny+1, id_dim_y)
            nc_stat = NF90_DEF_VAR(id_file, 'y', NF90_DOUBLE,  &
                                    id_dim_y, id_var_y)
            nc_stat = NF90_PUT_ATT(id_file, id_var_y, 'units', 'meters')

            !- Define output arrays
            nc_stat = NF90_DEF_VAR(id_file, 'phi', NF90_DOUBLE,           &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_phi)
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi,  &
                                    'long_name', 'geopotential at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi,  &
                                    'short_name', 'geopotential')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi, 'units', 'm2/s2')

            nc_stat = NF90_DEF_VAR(id_file, 'phi_ref', NF90_DOUBLE,       &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_phiref)
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref,  &
                                    'long_name',            &
                                    'reference geopotential at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref,  &
                                    'short_name', 'ref. geopotential')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref, 'units', 'm2/s2')

            nc_stat = NF90_DEF_VAR(id_file, 'zeta', NF90_DOUBLE,          &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_zeta)
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta,  &
                                    'long_name', 'vorticity at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta,  &
                                    'short_name', 'vorticity')
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta, 'units', '1/s')

            nc_stat = NF90_DEF_VAR(id_file, 'u', NF90_DOUBLE,             &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_u)
            nc_stat = NF90_PUT_ATT(id_file, id_var_u,  &
                                    'long_name',       &
                                    'u component of wind')
            nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'short_name', 'u')
            nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'units', 'm/s')

            nc_stat = NF90_DEF_VAR(id_file, 'v', NF90_DOUBLE,             &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_v)
            nc_stat = NF90_PUT_ATT(id_file, id_var_v,  &
                                    'long_name',       &
                                    'v component of wind')
            nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'short_name', 'v')
            nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'units', 'm/s')
            
            nc_stat = NF90_DEF_VAR(id_file, 's', NF90_DOUBLE,             &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_s)
            nc_stat = NF90_PUT_ATT(id_file, id_var_s,  &
                                    'long_name',       &
                                    'concentration of ash')
            nc_stat = NF90_PUT_ATT(id_file, id_var_s, 'short_name', 's')
            nc_stat = NF90_PUT_ATT(id_file, id_var_s, 'units', '%')       

            !- Leave define mode
            nc_stat = NF90_ENDDEF(id_file)

            !- Write axis to file
            ALLOCATE(netcdf_data_1d(0:nx))

            !- x axis
            DO  i = 0, nx
               netcdf_data_1d(i) = i * delta
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_x, netcdf_data_1d,  &
                                    start = (/1/), count = (/nx+1/))
            !- y axis
            DO  j = 0, ny
               netcdf_data_1d(j) = j * delta
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_y, netcdf_data_1d,  &
                                    start = (/1/), count = (/ny+1/))
                                    
!             DO  j = 0, ny
!                netcdf_data_1d(j) = ff(j)
!             ENDDO
!             nc_stat = NF90_PUT_VAR(id_file, id_var_ff, netcdf_data_1d,  &
!                                     start = (/1, do_count/), &
!                                     count = (/ny+1/))


            DEALLOCATE(netcdf_data_1d)


         !- Close NetCDF file
         CASE ('close')

            nc_stat = NF90_CLOSE(id_file)


         !- Write data arrays to file
         CASE ('write')

            ALLOCATE(netcdf_data_2d(0:nx,0:ny))

            do_count = do_count + 1

            !- Write time
            nc_stat = NF90_PUT_VAR(id_file, id_var_time, (/t_sim/),  &
                                    start = (/do_count/),               &
                                    count = (/1/))

            !- Write geopotential
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = phi(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_phi, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),          &
                                    count = (/nx+1, ny+1, 1/))

            !- Write reference geopotential
            tt =  INT(t_sim/3600.0/6.0)  ! time index of reference geopotential
           
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = phi_ref(j,i,tt) !(j,i,tt) 
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_phiref, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),             &
                                    count = (/nx+1, ny+1, 1/))

            !- Write vorticity
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = zeta(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_zeta, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),           &
                                    count = (/nx+1, ny+1, 1/))

            !- Write u
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = u(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_u, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),        &
                                    count = (/nx+1, ny+1, 1/))

            !- Write v
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = v(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_v, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),        &
                                    count = (/nx+1, ny+1, 1/))
                                    
            !- Write s
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = s(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_s, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),        &
                                    count = (/nx+1, ny+1, 1/))                                    

            DEALLOCATE(netcdf_data_2d)


         !- Print error message if unknown action selected
         CASE DEFAULT

            WRITE(*, '(A)') ' ** data_output: action "'//  &
                              TRIM(action)//'"unknown!'

      END SELECT

    CALL SYSTEM_CLOCK (count, count_rate)    
    diff5 = REAL(count)/REAL(count_rate) - diff5
    diff5_ges = diff5_ges + diff5  
      
   END SUBROUTINE data_output


END PROGRAM nwp_program
